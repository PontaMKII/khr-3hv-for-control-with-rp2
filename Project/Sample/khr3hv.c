#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <pthread.h>

#define Servo_Num 22		// Number of Servo-motor
#define BAUDRATE 115200		// Number of Servo-motor

struct servo_info {
	char PosID[Servo_Num];
	int Pos[Servo_Num];
	char SpeedID[Servo_Num];
	int Speed[Servo_Num];
};

/*--------------------------------------------------------------*/
/* Function Wait Time						*/
/* time : value to set wait time				*/
/*--------------------------------------------------------------*/
void WaitTime(unsigned int time){
	clock_t s = clock();
	clock_t c;
	do{
		if( ( c = clock() ) == ( clock_t ) -1 )
			break;
	}while( 1000 * (c-s) / CLOCKS_PER_SEC < time);
}

/*--------------------------------------------------------------*/
/* Function to set position of servo-motor			*/
/* Pos_ID : [1 ~ 22] ID number to set position			*/
/* Pos 	  : [3500 ~ 11500] value to set Position		*/
/*--------------------------------------------------------------*/
void Set_Servo_Pos(int Pos_ID,int Pos){

	char Servo_Pos_ID;
	//int i;

	Servo_Pos_ID = 0x80 + Pos_ID;

	char buf[2];
	buf[0] = (char)((Pos >> 7) & 0x7f);
	buf[1] = (char)(Pos & 0x7f);

	/*SerialPort Open*/
	int Serial_fd = serialOpen("/dev/ttyAMA0",BAUDRATE);

	serialPutchar(Serial_fd,Servo_Pos_ID);
	serialPutchar(Serial_fd,buf[0]);
	serialPutchar(Serial_fd,buf[1]);

	WaitTime(2);
}

/*--------------------------------------------------------------*/
/* Function to set speed of servo-motor				*/
/* Speed_ID  : [1 ~ 22] ID number to set speed			*/
/* Speed_Num : [1 ~ 127] value to set speed			*/
/*--------------------------------------------------------------*/
void Set_Servo_Speed(int Speed_ID,int Speed_Num){

	char Servo_Speed_ID;
	//int i;

	Servo_Speed_ID = 0x60 + Speed_ID;

	char buf[2];
	buf[0] = (char)(0x02);
	buf[1] = (char)(Speed_Num & 0x7f);

	/*SerialPort Open*/
	int Serial_fd = serialOpen("/dev/ttyAMA0",BAUDRATE);

	serialPutchar(Serial_fd,Servo_Speed_ID);
	serialPutchar(Serial_fd,buf[0]);
	serialPutchar(Serial_fd,buf[1]);

	WaitTime(1);
}

/*--------------------------------------------------------------*/
/* Function to set position of all servo-motors			*/
/* Postion : [3500 ~ 11500] value to set Position		*/
/*--------------------------------------------------------------*/
void Servo_Pos_All(int Postion, ...){

	int i,Pos = Postion;
	va_list args;
	va_start(args,Postion);

	for(i = 0; i < Servo_Num; i++){
		Set_Servo_Pos(i,Pos);
		Pos = va_arg(args,int);
	}
}

/*--------------------------------------------------------------*/
/* Function to set speed of all servo-motors			*/
/* Speed : [1 ~ 127] value to set speed				*/
/*--------------------------------------------------------------*/
void Servo_Speed_All(int Speed){
	int i;
	for(i = 0; i < Servo_Num; i++){
		Set_Servo_Speed(i,Speed);
	}
}

/*--------------------------------------------------------------*/
/* Function to make robot the initial position 			*/
/* 								*/
/*--------------------------------------------------------------*/
void Init_Pos(){
	int Servo_Init_Pos[Servo_Num] = {7500,6000,5500,7500,9500,7500,7550,7500,5600,7000,7550,9000,9500,7500,5500,7500,7450,7500,9300,8000,7450,7500};
	int i;
	for(i = 0; i < Servo_Num; i++){
		Set_Servo_Pos(i,Servo_Init_Pos[i]);
	}
}

/*以下、スレッド*/

//センサ取得
void* Sensor_Get(void* args){

}

//ZMP取得
void* ZMP_Calc(void* args){

}

//ロボット動作
void* Move(void* args){

}

/*--------------------------------------------------------------*/
/* main                                                         */
/*--------------------------------------------------------------*/
int main(int argc,char *argv[]){
	while(1){
		/**/

	}
}
