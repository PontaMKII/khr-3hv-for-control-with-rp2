#include <stdio.h>
#include "control.h"
#include "sensor.h"
#include "servo.h"
#include "struct.h"
#include "struct2.h"
#include "struct3.h"

struct ZMP zmp[3] = {
	{ 0,0,0,30,54,0 },
	{ 0,0,0,30,54,0 },
	{ 0,0,0,0,0,0 },
};

void Set_Target_ZMP(){
  //left
  zmp[0].tx = 30;
  zmp[0].ty = 54;

  //Right
  zmp[1].tx = 30;
  zmp[1].ty = 54;

  //Both
  zmp[2].tx = 0;
  zmp[2].ty = 0;
}

void ZMP_Calc(){

  //Left
  zmp[0].x = (34 * sensor[0].Value + (-1*34) * sensor[1].Value + (-1*34) * sensor[2].Value + 34 * sensor[3].Value) / (sensor[0].Value + sensor[1].Value + sensor[2].Value + sensor[3].Value);
  zmp[0].y = (57 * sensor[0].Value + 57 * sensor[1].Value + (-1*57) * sensor[2].Value + (-1*57) * sensor[3].Value) / (sensor[0].Value + sensor[1].Value + sensor[2].Value + sensor[3].Value);

  //Right
  zmp[1].x = (34 * sensor[4].Value + (-1*34) * sensor[5].Value + (-1*34) * sensor[6].Value + 34 * sensor[7].Value) / (sensor[4].Value + sensor[5].Value + sensor[6].Value + sensor[7].Value);
  zmp[1].y = (57 * sensor[4].Value + 57 * sensor[5].Value + (-1*57) * sensor[6].Value + (-1*57) * sensor[7].Value) / (sensor[4].Value + sensor[5].Value + sensor[6].Value + sensor[7].Value);

  //Both
  zmp[2].x = (34 * sensor[0].Value + (-1*34) * sensor[1].Value + (-1*34) * sensor[2].Value + 34 * sensor[3].Value + 34 * sensor[4].Value + (-1*34) * sensor[5].Value + (-1*34) * sensor[6].Value + 34 * sensor[7].Value) / (sensor[0].Value + sensor[1].Value + sensor[2].Value + sensor[3].Value + sensor[4].Value + sensor[5].Value + sensor[6].Value + sensor[7].Value);
  zmp[2].y = (57 * sensor[0].Value + 57 * sensor[1].Value + (-1*57) * sensor[2].Value + (-1*57) * sensor[3].Value + 57 * sensor[4].Value + 57 * sensor[5].Value + (-1*57) * sensor[6].Value + (-1*57) * sensor[7].Value) / (sensor[0].Value + sensor[1].Value + sensor[2].Value + sensor[3].Value + sensor[4].Value + sensor[5].Value + sensor[6].Value + sensor[7].Value);

}

void ZMP_Control(){

	//Left
	if(zmp[0].x < zmp[0].tx){
		servo[0].CalibrationPos += 10;
		servo[7].CalibrationPos += 10;
	}else if(zmp[0].x > zmp[0].tx){
		servo[0].CalibrationPos -= 10;
		servo[7].CalibrationPos -= 10;
	}

	if(zmp[0].y < zmp[0].ty){
		servo[9].CalibrationPos += 10;
	}else if(zmp[0].y > zmp[0].ty){
		servo[9].CalibrationPos -= 10;
	}

	//Right
	if(zmp[1].x < zmp[1].tx){
		servo[1].CalibrationPos += 10;
		servo[5].CalibrationPos += 10;
	}else if(zmp[1].x > zmp[1].tx){
		servo[1].CalibrationPos -= 10;
		servo[5].CalibrationPos -= 10;
	}

	if(zmp[1].y < zmp[1].ty){
		servo[4].CalibrationPos += 10;
	}else if(zmp[1].y > zmp[1].ty){
		servo[4].CalibrationPos -= 10;
	}
	/*
	//Both
	if(zmp[2].x < zmp[2].tx){
		servo[1].CalibrationPos += 10;
		servo[5].CalibrationPos += 10;
	}else if(zmp[2].x > zmp[2].tx){
		servo[1].CalibrationPos -= 10;
		servo[5].CalibrationPos -= 10;
	}

	if(zmp[2].y < zmp[2].ty){
		servo[4].CalibrationPos += 10;
	}else if(zmp[2].y > zmp[2].ty){
		servo[4].CalibrationPos -= 10;
	}
	*/
}
