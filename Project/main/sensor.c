#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <termios.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <stdlib.h>
#include <pthread.h>
#include "sensor.h"
#include "control.h"
#include "struct2.h"

#define DEV_NAME    "/dev/ttyUSB0"     // デバイスファイル名
#define BAUD_RATE    B115200           // RS232C通信ボーレート
#define BUFF_SIZE    68                // 適当


//Serial Init
void Serial_Init(int fd)
{
    struct termios tio;
    memset(&tio,0,sizeof(tio));
    tio.c_cflag = CS8 | CLOCAL | CREAD;
    tio.c_cc[VTIME] = 100;
    // ボーレートの設定
    cfsetispeed(&tio,BAUD_RATE);
    cfsetospeed(&tio,BAUD_RATE);
    // デバイスに設定を行う
    tcsetattr(fd,TCSANOW,&tio);
}

//Unit Conversion
double Unit_Conversion(double voltage){
	double N = 0;
	N = 0.0000003 * pow(voltage,6.0) - 0.00007 * pow(voltage,5.0) + 0.0084 * pow(voltage,4.0) - 0.4934 * pow(voltage,3.0) + 15.817 * pow(voltage,2.0) - 258.29 * voltage + 1661.6;
	return N;
}

void Sensor_Get(){
//void* Sensor_Get(void* args){

  int fd,fc=0;
	int i,n,p,j=0;
	int len,num,count = 0;                              	// データ数（バイト）
	unsigned char buffer[BUFF_SIZE],in_data[BUFF_SIZE]; 	// データバッファ
	fd = open(DEV_NAME,O_RDWR);         			// デバイスファイル（シリアルポート）オープン

	if(fd<0){                                   		// デバイスの open() に失敗したら
		//perror(argv[1]);
    printf("USB Device Not Found! Check Your USB Port!\n");
		//exit(1);
	}

	Serial_Init(fd);                                    	// シリアルポートの初期化
	//Main Loop
	//FILE *fp;

	//fp = fopen("sensor_log.txt","w");

	//while(1){
          //Read Wait
        	len = read(fd,buffer,BUFF_SIZE);
          /*
        	if(len<=0){
            		continue;
        	}
          */

        	for(i=0; i<len; i++){
        		in_data[j++] = buffer[i];
        	}

      		if(len < 0){
            printf("ERORR DATA!\n");
//      			exit(1);
      		}

          if((in_data[j - 1] == 0x0a) || (in_data[j - 1] == 0)){
      			if(BUFF_SIZE <= j){
              in_data[j - 1] = 0x0a;
              in_data[j] = 0;

      				for(n = 0; n < BUFF_SIZE; n++){
      					switch(atoi(&in_data[n])){
      						//Left Sensor
      						case -1:
      							sensor[0].Value = atoi(&in_data[n+2]) + sensor[0].Calibration;
      							//sensor[0].Load = Unit_Conversion(sensor[0].Value);
      							if(sensor[0].Value < 0){
      								sensor[0].Value = -1 * sensor[0].Value;
      							}
      							break;

      						case -2:
      							sensor[1].Value = atoi(&in_data[n+4]) + sensor[1].Calibration;
      							//sensor[1].Load = Unit_Conversion(sensor[1].Value);
      							if(sensor[1].Value < 0){
      								sensor[1].Value = -1 * sensor[1].Value;
      							}
      							break;

      						case -3:
      							sensor[2].Value = atoi(&in_data[n+4]) + sensor[2].Calibration;
      							//sensor[2].Load = Unit_Conversion(sensor[2].Value);
      							if(sensor[2].Value<0){
      								sensor[2].Value = -1 * sensor[2].Value;
      							}
      							break;

      						case -4:
      							sensor[3].Value = atoi(&in_data[n+4]) + sensor[3].Calibration;
      							//sensor[3].Load = Unit_Conversion(sensor[3].Value);
      							if(sensor[3].Value<0){
      								sensor[3].Value = -1 * sensor[3].Value;
      							}
      							break;
      						//Right Sensor
      						case -5:
      							sensor[4].Value = atoi(&in_data[n+4]) + sensor[4].Calibration;
      							//sensor[4].Load = Unit_Conversion(sensor[4].Value);
      							if(sensor[4].Value<0){
      								sensor[4].Value = -1 * sensor[4].Value;
      							}
      							break;

      						case -6:
      							sensor[5].Value = atoi(&in_data[n+4]) + sensor[5].Calibration;
      							//sensor[5].Load = Unit_Conversion(sensor[5].Value);
      							if(sensor[5].Value<0){
      								sensor[5].Value = -1 * sensor[5].Value;
      							}
      							break;

      						case -7:
      							sensor[6].Value = atoi(&in_data[n+4]) + sensor[6].Calibration;
      							//sensor[6].Load = Unit_Conversion(sensor[6].Value);
      							if(sensor[6].Value<0){
      								sensor[6].Value = -1 * sensor[6].Value;
      							}
      							break;

      						case -8:
      							sensor[7].Value = atoi(&in_data[n+4]) + sensor[7].Calibration;
      							//sensor[7].Load = Unit_Conversion(sensor[7].Value);
      							if(sensor[7].Value<0){
      								sensor[7].Value = -1 * sensor[7].Value;
      							}
      							break;
      						default:
      							break;
      					}
      				}
      			}
        j = 0;
    }
	//}
}

void Set_S(){
	int i;
	for(i = 0; i < 8; i++){
		sensor[i].Value = (rand()% 6 + 1)*100;
		//printf("%d\n", sensor[i].Value);
	}
}
