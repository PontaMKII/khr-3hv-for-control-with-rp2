#include <stdio.h>
#include "motion.h"
#include "servo.h"
#include "struct.h"
/*
struct ROBOT{
  int mt;
  int tp[22];
};
*/

/*--------------------------------------------------------------*/
/* Function to make robot the initial position 			*/
/* 								*/
/*--------------------------------------------------------------*/
void Init_Pos(){
  printf("Init Position Now\n");
	//Koeda robot--------------------  ID0  ID1  ID2  ID3  ID4  ID5  ID6  ID7  ID8  ID9  ID10 ID11 ID12 ID13 ID14 ID15 ID16 ID17 ID18 ID19 ID20 ID21
	//int Servo_Init_Pos[Servo_Num] = {7500,6000,5500,7500,9500,7500,7550,7500,5600,7000,7550,9000,9500,7500,5500,7500,7450,7500,9300,8000,7450,7500};
	//Sugimoto robot-----------------ID0  ID1  ID2  ID3  ID4  ID5  ID6  ID7  ID8  ID9  ID10 ID11 /ID12 ID13 ID14 ID15 ID16 ID17 ID18 ID19 ID20 ID21
	int Servo_Init_Pos[Servo_Num] = {7550,7450,7500,9100,8175,7450,7600,7500,5800,6825,7500,7025,7500,7500,7500,7500,7500,7500,7500,7500,7500,7500};

	int i;
	for(i = 0; i < Servo_Num; i++){
    servo[i].Pos = Servo_Init_Pos[i];
    //printf("%d\n",Servo_Init_Pos[i]);
		Send_PosData(i,servo[i].Pos);
	}

}
/*
void Set_Pos(struct ROBOT *R_Pos){

  int i;

  for(i = 0; i < 22; i++){
  //  servo[i].Pos = R_Pos->tp;
  //  servo[i].MTime = R_Pos->mt;
    printf("%d\n", R_Pos->tp);
    printf("%d\n", R_Pos->mt);
    R_Pos++;
  }

  Acc_Control();

}
*/

void Set_Pos(int p1,int p2,int p3,int p4,int p5,int p6,int p7,int p8,int p9,int p10,int p11,int p12,int p13,int p14,int p15,int p16,int p17,int p18,int p19,int p20,int p21,int p22){
  servo[0].TragetPos = p1 + servo[0].CalibrationPos;
  servo[1].TragetPos = p2 + servo[1].CalibrationPos;
  servo[2].TragetPos = p3 + servo[2].CalibrationPos;
  servo[3].TragetPos = p4 + servo[3].CalibrationPos;
  servo[4].TragetPos = p5 + servo[4].CalibrationPos;
  servo[5].TragetPos = p6 + servo[5].CalibrationPos;
  servo[6].TragetPos = p7 + servo[6].CalibrationPos;
  servo[7].TragetPos = p8 + servo[7].CalibrationPos;
  servo[8].TragetPos = p9 + servo[8].CalibrationPos;
  servo[9].TragetPos = p10 + servo[9].CalibrationPos;
  servo[10].TragetPos = p11 + servo[10].CalibrationPos;
  servo[11].TragetPos = p12 + servo[11].CalibrationPos;
  servo[12].TragetPos = p13 + servo[12].CalibrationPos;
  servo[13].TragetPos = p14 + servo[13].CalibrationPos;
  servo[14].TragetPos = p15 + servo[14].CalibrationPos;
  servo[15].TragetPos = p16 + servo[15].CalibrationPos;
  servo[16].TragetPos = p17 + servo[16].CalibrationPos;
  servo[17].TragetPos = p18 + servo[17].CalibrationPos;
  servo[18].TragetPos = p19 + servo[18].CalibrationPos;
  servo[19].TragetPos = p20 + servo[19].CalibrationPos;
  servo[20].TragetPos = p21 + servo[20].CalibrationPos;
  servo[21].TragetPos = p22 + servo[21].CalibrationPos;

/*
  int i;
  for(i = 0; i < 22; i++){
    printf("%d\n", servo[i].TragetPos);
  }
*/
  Acc_Control();
  
  WaitTime(500);

}


void Shuffling_Walk(){
  //struct ROBOT R_Pos[22];
/*
  struct ROBOT R_Pos[22] = {
    { 1000,7550},
    { 1000,7450},
    { 1000,7500},
    { 1000,9100},
    { 1000,8175},
    { 1000,7450},
    { 1000,7600},
    { 1000,7500},
    { 1000,5800},
    { 1000,6825},
    { 1000,7500},
    { 1000,7025},
    { 1000,7500},
    { 1000,7500},
    { 1000,7500},
    { 1000,7500},
    { 1000,7500},
    { 1000,7500},
    { 1000,7500},
    { 1000,7500},
    { 1000,7500},
    { 1000,7500},
  };

  Set_Pos(R_Pos);
*/
  //初期ポーズ
  Set_Pos(
    7550,
    7450,
    7500,
    9100,
    8175,
    7450,
    7600,
    7500,
    5800,
    6825,
    7500,
    7025,
    7500,
    7500,
    7500,
    7500,
    7500,
    7500,
    7500,
    7500,
    7500,
    7500
  );


  //ポーズ1
  Set_Pos(
    8550,
    8450,
    8500,
    8100,
    8175,
    8450,
    8600,
    8500,
    8800,
    8825,
    8500,
    8025,
    8500,
    8500,
    8500,
    8500,
    8500,
    8500,
    8500,
    8500,
    8500,
    8500
  );

  //ポーズ2
  Set_Pos(
    7550,
    7450,
    7500,
    9100,
    8175,
    7450,
    7600,
    7500,
    5800,
    6825,
    7500,
    7025,
    7500,
    7500,
    7500,
    7500,
    7500,
    7500,
    7500,
    7500,
    7500,
    7500
  );

  //ポーズ3

  //ポーズ4

  //ポーズ5

  //ポーズ6

}

void ZMP_Shuffling_Walk(){

}
