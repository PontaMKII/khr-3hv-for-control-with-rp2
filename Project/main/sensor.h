#define Sensor_Num    8

//構造体
struct SENSOR {
  int Value;
	int Calibration;
  double Load;
};


//  関数のプロトタイプ宣言
extern void Serial_Init(int fd);
extern double Unit_Conversion(double voltage);
//extern void Init_Sensor_Info();
extern void Sensor_Get();
extern void Set_S();
//extern void* Sensor_Get(void* args);
