#define Num 3

//構造体
struct ZMP {
  int x;
	int y;
  int z;
  int tx;
  int ty;
  int tz;
};

//  関数のプロトタイプ宣言
extern void ZMP_Calc();
extern void Set_Target_ZMP();
extern void ZMP_Control();
