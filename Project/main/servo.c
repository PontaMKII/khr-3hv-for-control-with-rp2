#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
//#include <wiringPi.h>
//#include <wiringSerial.h>
//#include <wiringPiI2C.h>
#include "servo.h"
#include "motion.h"
#include "struct.h"
/*
struct SERVO servo[Servo_Num] = {
	{ 0,7550,10000,0,Default_Speed,1000,0 },
	{ 1,7450,10000,0,Default_Speed,1000,0 },
	{ 2,7500,10000,0,Default_Speed,1000,0 },
	{ 3,9100,10000,0,Default_Speed,1000,0 },
	{ 4,8175,10000,0,Default_Speed,1000,0 },
	{ 5,7450,10000,0,Default_Speed,1000,0 },
	{ 6,7600,10000,0,Default_Speed,1000,0 },
	{ 7,7500,10000,0,Default_Speed,1000,0 },
	{ 8,5800,10000,0,Default_Speed,1000,0 },
	{ 9,6825,10000,0,Default_Speed,1000,0 },
	{ 10,7500,10000,0,Default_Speed,1000,0 },
	{ 11,7025,10000,0,Default_Speed,1000,0 },
	{ 12,7500,10000,0,Default_Speed,1000,0 },
	{ 13,7500,7500,0,Default_Speed,1000,0 },
	{ 14,7500,7500,0,Default_Speed,1000,0 },
	{ 15,7500,7500,0,Default_Speed,1000,0 },
	{ 16,7500,7500,0,Default_Speed,1000,0 },
	{ 17,7500,7500,0,Default_Speed,1000,0 },
	{ 18,7500,7500,0,Default_Speed,1000,0 },
	{ 19,7500,7500,0,Default_Speed,1000,0 },
	{ 20,7500,7500,0,Default_Speed,1000,0 },
	{ 21,7500,7500,0,Default_Speed,1000,0 },
};
*/
/*
struct SERVO servo[Servo_Num] = {
	{ 0,0,0,0,Default_Speed,1000,0 },
	{ 1,0,0,0,Default_Speed,1000,0 },
	{ 2,0,0,0,Default_Speed,1000,0 },
	{ 3,0,0,0,Default_Speed,1000,0 },
	{ 4,0,0,0,Default_Speed,1000,0 },
	{ 5,0,0,0,Default_Speed,1000,0 },
	{ 6,0,0,0,Default_Speed,1000,0 },
	{ 7,0,0,0,Default_Speed,1000,0 },
	{ 8,0,0,0,Default_Speed,1000,0 },
	{ 9,0,0,0,Default_Speed,1000,0 },
	{ 10,0,0,0,Default_Speed,1000,0 },
	{ 11,0,0,0,Default_Speed,1000,0 },
	{ 12,0,0,0,Default_Speed,1000,0 },
	{ 13,0,0,0,Default_Speed,1000,0 },
	{ 14,0,0,0,Default_Speed,1000,0 },
	{ 15,0,0,0,Default_Speed,1000,0 },
	{ 16,0,0,0,Default_Speed,1000,0 },
	{ 17,0,0,0,Default_Speed,1000,0 },
	{ 18,0,0,0,Default_Speed,1000,0 },
	{ 19,0,0,0,Default_Speed,1000,0 },
	{ 20,0,0,0,Default_Speed,1000,0 },
	{ 21,0,0,0,Default_Speed,1000,0 },
};
*/
#define BAUDRATE 115200		// BAUDRATE of Servo-motor

void WaitTime(unsigned int time){
	clock_t s = clock();
	clock_t c;

	do{
		if((c = clock())==(clock_t)-1)
			break;
	}while(1000*(c-s)/CLOCKS_PER_SEC<time);
}

/*--------------------------------------------------------------*/
/* Function to set position of servo-motor			*/
/* Pos_ID : [1 ~ 22] ID number to set position			*/
/* Pos 	  : [3500 ~ 11500] value to set Position		*/
/*--------------------------------------------------------------*/
void Send_PosData(int Servo_ID,int Target_Pos){

	char Servo_Pos_ID = 0x80 + Servo_ID;
	char Pos_buf[2];
	Pos_buf[0] = (char)((Target_Pos >> 7) & 0x7f);
	Pos_buf[1] = (char)(Target_Pos & 0x7f);

	/*SerialPort Open*/
  /*
	int Serial_fd = serialOpen("/dev/ttyAMA0",BAUDRATE);

	serialPutchar(Serial_fd,Servo_Pos_ID);
	serialPutchar(Serial_fd,Pos_buf[0]);
	serialPutchar(Serial_fd,Pos_buf[1]);
  */

	WaitTime(2);
}

/*
void Acc_Control(int Servo_ID,long start,long stop,long m_time){
	int i ;
	for(i = 0; i<Servo_Num; i++){
		long Now_Pos,time = 0;

	  // 加速度計算
		float a = 2 * (((float)servo[i].TargetPos-(float)servo[i].Pos)/((float)servo[i].MTime*(float)servo[i].MTime));
		//float a = 2 * (((float)stop-(float)start)/((float)m_time*(float)m_time));

	  //絶対値化
	  if(a < 0){ a *= -1; }

	  //printf("Start\n");
	  //加速制御
	  do{
			Now_Pos = a * time * time + start;
	    Send_PosData(Servo_ID,Now_Pos);
			time += 50;
		}while(time < (m_time/2));

	  //減速制御
		do{
			Now_Pos = -1 * a * (m_time - time) * (m_time - time) + stop;
	    Send_PosData(Servo_ID,Now_Pos);
			time += 50;
		}while(time <= m_time);
	}
  //printf("Stop\n");
}
*/

//目標角度を入力すると、ほぼ同期しながらサーボが動くはず
void Acc_Control(){

	float a,n;
	int i , count = 0;

	//servo[count].Timerを初期化
	for(i = 0; i < Servo_Num; i++){
		servo[i].Timer = 0;
	}

	do{
		for(i = 0; i < Servo_Num; i++){
			if((servo[i].TragetPos - servo[i].Pos) != 0 ){

				// 加速度計算
				a = 2 * (((float)servo[i].TragetPos+(float)servo[i].CalibrationPos-(float)servo[i].Pos)/((float)servo[i].MTime*(float)servo[i].MTime));

				//絶対値化
				if(a < 0){ a *= -1; }
				if((servo[i].TragetPos - servo[i].Pos)>0){ n = 1;}else{ n = -1;}

				//加速制御
				servo[i].Pos = n * a * servo[i].Timer * servo[i].Timer + servo[i].Pos;
		    Send_PosData(i,servo[i].Pos);
			}
			servo[i].Timer += 50;
		}
		count += 1;
	}while(servo[count].Timer < (servo[count].MTime/2));

	count = 0;
	/*
	for(i = 0; i<22;i++){
		//servo[i].Pos = 0;
		printf("%d\n",servo[i].Pos);
	}
	*/
	do{
		for(i = 0; i < Servo_Num; i++){
			if((servo[i].TragetPos - servo[i].Pos) != 0 ){

				// 加速度計算
				a = 2 * (((float)servo[i].TragetPos+(float)servo[i].CalibrationPos-(float)servo[i].Pos)/((float)servo[i].MTime*(float)servo[i].MTime));

				//絶対値化
				if(a < 0){ a *= -1; }
				if((servo[i].TragetPos - servo[i].Pos)>0){ n = -1;}else{ n = 1;}

				//減速制御
				servo[i].Pos = n * a * (servo[i].MTime - servo[i].Timer) * (servo[i].MTime - servo[i].Timer) + servo[i].TragetPos;
				Send_PosData(i,servo[i].Pos);
			}
			servo[i].Timer += 50;
		}
		count += 1;
	}while(servo[count].Timer <= servo[count].MTime);
	/*
	for(i = 0; i<22;i++){
		//servo[i].Pos = 0;
		printf("%d\n",servo[i].Pos);
	}
	*/
}

/*--------------------------------------------------------------*/
/* Function to set speed of servo-motor				*/
/* Speed_ID  : [1 ~ 22] ID number to set speed			*/
/* Speed_Num : [1 ~ 127] value to set speed			*/
/*--------------------------------------------------------------*/
void Set_Servo_Speed(int Servo_ID,int Speed_Num){
	// ID0  ID1  ID2  ID3  ID4  ID5  ID6  ID7  ID8  ID9  ID10 ID11 ID12 ID13 ID14 ID15 ID16 ID17 ID18 ID19 ID20 ID21
	//{0x60,0xC1,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8,0xC9,0xCA,0xCB,0xCC,0xCD,0xCE,0xCF,0xD0,0xD1,0xD2,0xD3,0xD4,0xD5};
	char Servo_Speed_ID = 0x60 + Servo_ID;
	char Speed_buf[2];

	Speed_buf[0] = (char)(0x02);
	Speed_buf[1] = (char)(Speed_Num & 0x7f);

	/*SerialPort Open*/
  /*
	int Serial_fd = serialOpen("/dev/ttyAMA0",BAUDRATE);

	serialPutchar(Serial_fd,Servo_Speed_ID);
	serialPutchar(Serial_fd,Speed_buf[0]);
	serialPutchar(Serial_fd,Speed_buf[1]);
  */
	WaitTime(2);
}

/*--------------------------------------------------------------*/
/* Function to set position of all servo-motors			*/
/* Postion : [3500 ~ 11500] value to set Position		*/
/*--------------------------------------------------------------*/
void Servo_Pos_All(int Postion, ...){
	int i,Pos = Postion;
	va_list args;
	va_start(args,Postion);
	for(i = 0; i < Servo_Num; i++){
		Send_PosData(i,Pos);
		Pos = va_arg(args,int);
	}
}

/*--------------------------------------------------------------*/
/* Function to set speed of all servo-motors			*/
/* Speed : [1 ~ 127] value to set speed				*/
/*--------------------------------------------------------------*/
void Servo_Speed_All(int Speed){
	int i;
	for(i = 0; i < Servo_Num; i++){
		Set_Servo_Speed(i,Speed);
	}
}
