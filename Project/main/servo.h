#define Servo_Num 22
#define Default_Speed 30

//構造体
struct SERVO {
  int ID;
	int Pos;
  int TragetPos;
  int CalibrationPos;
  int Speed;
  int MTime;
  long Timer;
};

//  関数のプロトタイプ宣言
extern void WaitTime(unsigned int time);
//extern void Init_Servo_Info();
extern void Send_PosData(int Servo_ID,int Target_Pos);
extern void Acc_Control();
extern void Set_Servo_Speed(int Servo_ID,int Speed_Num);
