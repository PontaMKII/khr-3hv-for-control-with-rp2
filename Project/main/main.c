#include <stdio.h>
#include <pthread.h>
#include "servo.h"
#include "sensor.h"
#include "motion.h"
#include "control.h"
#include "struct.h"
#include "struct2.h"
#include "struct3.h"

struct SERVO servo[Servo_Num] = {
	{ 0,0,0,0,Default_Speed,1000,0 },
	{ 1,0,0,0,Default_Speed,1000,0 },
	{ 2,0,0,0,Default_Speed,1000,0 },
	{ 3,0,0,0,Default_Speed,1000,0 },
	{ 4,0,0,0,Default_Speed,1000,0 },
	{ 5,0,0,0,Default_Speed,1000,0 },
	{ 6,0,0,0,Default_Speed,1000,0 },
	{ 7,0,0,0,Default_Speed,1000,0 },
	{ 8,0,0,0,Default_Speed,1000,0 },
	{ 9,0,0,0,Default_Speed,1000,0 },
	{ 10,0,0,0,Default_Speed,1000,0 },
	{ 11,0,0,0,Default_Speed,1000,0 },
	{ 12,0,0,0,Default_Speed,1000,0 },
	{ 13,0,0,0,Default_Speed,1000,0 },
	{ 14,0,0,0,Default_Speed,1000,0 },
	{ 15,0,0,0,Default_Speed,1000,0 },
	{ 16,0,0,0,Default_Speed,1000,0 },
	{ 17,0,0,0,Default_Speed,1000,0 },
	{ 18,0,0,0,Default_Speed,1000,0 },
	{ 19,0,0,0,Default_Speed,1000,0 },
	{ 20,0,0,0,Default_Speed,1000,0 },
	{ 21,0,0,0,Default_Speed,1000,0 },
};

struct SENSOR sensor[Sensor_Num] = {
	{ 0,0,0.0 },
	{ 0,0,0.0 },
	{ 0,0,0.0 },
	{ 0,0,0.0 },
	{ 0,0,0.0 },
	{ 0,0,0.0 },
	{ 0,0,0.0 },
	{ 0,0,0.0 },
};

pthread_mutex_t sensor_mutex;  // sensor Mutex
pthread_mutex_t move_mutex;  // move Mutex

// Sensor Thread
void* Sensor( void* args ){

	// 他のスレッドが mutex を返却するを待つ
	// mutex が返却されたら mutex を取得してブロック
	pthread_mutex_lock( &sensor_mutex );

	// ここからクリティカルセクション

	printf("Thread Sensor\n");
	Sensor_Get();
	Set_S();
	ZMP_Calc();
	ZMP_Control();
	//Acc_Control();
	Shuffling_Walk();
	/*
	int i = 0;

	for(i = 0; i<22;i++){
		//servo[i].Pos = 0;
		printf("%d\n",servo[i].Pos);
	}

	for(i = 0; i<8;i++){
		//servo[i].Pos = 0;
		printf("%d\n",sensor[i].Value);
	}

	for(i = 0; i<3;i++){
		//servo[i].Pos = 0;
		printf("%d\n",zmp[i].x);
	}

	for(i = 0; i<22;i++){
		//servo[i].Pos = 0;
		printf("%d\n",servo[i].CalibrationPos);
	}
	*/


	// ここまでクリティカルセクション

	// mutex返却
	pthread_mutex_unlock( &sensor_mutex );

	return NULL;
}

// Move Thread
void* Move( void* args ){

	// 他のスレッドが mutex を返却するを待つ
	// mutex が返却されたら mutex を取得してブロック
	pthread_mutex_lock( &move_mutex );

	// ここからクリティカルセクション

	printf("Thread Move\n");
	//Shuffling_Walk();
	//Acc_Control();
	/*
	int i = 0;

	for(i = 0; i<22;i++){
		//servo[i].Pos = 0;
		printf("%d\n",servo[i].Pos);
	}

	for(i = 0; i<8;i++){
		//servo[i].Pos = 0;
		printf("%d\n",sensor[i].Value);
	}

	for(i = 0; i<3;i++){
		//servo[i].Pos = 0;
		printf("%d\n",zmp[i].x);
	}
	*/

	/*
	int i = 0;

	for(i = 0; i<12;i++){
		//servo[i].Pos = 0;
		printf("%d\n",servo[i].Pos);
	}

	for(i = 0; i<8;i++){
		//servo[i].Pos = 0;
		printf("%d\n",sensor[i].Value);
	}

	for(i = 0; i<3;i++){
		//servo[i].Pos = 0;
		printf("%d\n",zmp[i].x);
	}

	for(i = 0; i<6;i++){
		//servo[i].Pos = 0;
		printf("%d\n",servo[i].CalibrationPos);
	}
	*/
	// ここまでクリティカルセクション

	// mutex返却
	pthread_mutex_unlock( &move_mutex );

	return NULL;
}

int main(){

  pthread_t th1, th2;

  printf("Init Start\n");

  //Init_Servo_Info();
  //Init_Sensor_Info();

	//Send_PosData(5,1000);
	Init_Pos();
	Set_Target_ZMP();

  while (1) {
    printf("Main Loop\n");

		/*
		int i = 0;

		for(i = 0; i<22;i++){
			//servo[i].Pos = 0;
			printf("%d\n",servo[i].Pos);
		}

		for(i = 0; i<8;i++){
			//servo[i].Pos = 0;
			printf("%d\n",sensor[i].Value);
		}

		for(i = 0; i<3;i++){
			//servo[i].Pos = 0;
			printf("%d\n",zmp[i].x);
		}
		*/

    // mutex1,2 作成
    pthread_mutex_init( &sensor_mutex, NULL );
    //pthread_mutex_init( &move_mutex, NULL );

    // スレッド1,2の作成と起動
    pthread_create( &th1, NULL, Sensor,(void *)NULL );
    //pthread_create( &th2, NULL, Move, (void *)NULL );

    // スレッド終了を待つ
    pthread_join( th1, NULL );
    //pthread_join( th2, NULL );

    // mutex 開放
    pthread_mutex_destroy( &sensor_mutex );
    //pthread_mutex_destroy( &move_mutex );

  }
  return 0;
}
